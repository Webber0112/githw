ls -lRASF | awk 'BEGIN{ i=0; array[0]=""; } \
{ if($9!~"/"){ if(i<=5){ array[i]=$5" "$9; i++; } FCOUNT+=1; SUM+=$5; } \
else{ DCOUNT+=1; } \
} END { \
for(k=1; k<=5; k++){ print k": "array[k] } \
print "Number of directories: "DCOUNT"\n" \
"Number of files: "FCOUNT"\n" \
"Total size: "SUM"\n" }'
