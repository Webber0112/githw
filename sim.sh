showMenu(){
	dialog --title "SYS INFO" --menu "choose" 20 50 4 \
	1 "CPU INFO" 2 "MEMORY INFO" 3 "NETWORK INFO" 4 "FILE BROWSER" \
	5 "CPU USAGE" 2>input
	path=$(pwd)
	if [ "$?" = "0"  ]
	then
		chosen=$(cat input)
		case $chosen in \
 			1)showCpu;;
			2)showMemory;;
			3)showNet;;
			4)showFile $path;;
			5)showCpuUse;;
		esac
		rm -f input
	else
		echo "Exit"
	fi
}

showCpu(){
	dialog --title "CPU INFO"  --msgbox \
	"$(sysctl hw.model hw.machine hw.ncpu|sed 's/hw./CPU /g')" 20 50
	if [ "$?" = "0" ]
	then
		showMenu
	fi
}

showMemory(){
	total=$(sysctl hw.realmem|awk '{size=$2/1024/1024/1024; print size}')
	freet=$(top | grep "Free" | sed 's/M//g'  | \
	awk 'NR==1{print $10}')
	ansfree=$(($freet/1024))
	used=$(($(sysctl -n hw.realmem)-$freet*1024*1024))
	calused=$(($used/1024/1024))
	t=$(($(sysctl -n hw.realmem)+0))
	p="$(echo "scale=1;$used*100/$t" |bc)"
	percent=$(echo "$p" | cut -d '.' -f 1)

	dialog --title "MEMORY INFO" --gauge \
	"Total: $total GB \
	\nUsed: $calused MB\nFree: \
	$(top | grep "Free" | sed 's/M//g' | \
	awk 'NR==1{fs=$10/1024;print fs}') GB" 20 50 $percent
	
	while [ true ]
	do
	read -t 5 -n 1
	if [ "$?" = "0" ]
	then
		showMenu
		exit	
	else
		showMenu
		exit
	fi
	done
}

showNet(){
	devicecount=$(($(ifconfig -l|grep -o ' '|wc -l)+1))
	device=""
	alldevice=""
	for i in $(seq 1 $devicecount)
	do
		device=$(ifconfig -l| cut -d ' ' -f $i)
		alldevice=$alldevice$i" "$device" "
	done
	
	dialog --title "NETWORK INFO" --menu "Select" 20 50 5 \
	$alldevice 2>netinput

	if [ "$?" = "1" ]
	then
		showMenu
	else
		netchosen=$(($(cat netinput)+0))
		device=$(ifconfig -l| cut -d ' ' -f $netchosen)
		showNetDetail $device
		rm -f netinput
	fi
}

showNetDetail(){
	netmask=$(ifconfig $1 | grep 'netmask' | awk '{ \
	if(NR==1)print $4 }')
	
	ipv4=$(ifconfig $1 | grep 'inet' | awk '{ \
	if($2!~":")print $2 }')
	
	mac=$(ifconfig $1 | grep 'ether' | awk '{ \
	if(NR==1)print $2 }')
	
	dialog --title "NETWORK DETAIL" --msgbox \
	"netmask: $netmask\n IPv4__: $ipv4\n  Mac__: $mac" 20 50
	
	if [ "$?" = "0" ]
	then
		showNet
	fi
}

showFile(){
	path=$1
	allitem=""
	item=""
	mime=""
	list=$(ls $path | cut -d ' ' -f 1 | wc -l)
	for i in $(seq 1 $list)
	do
		item=$(ls $path | awk "{if(NR==$i)print}")
		mime=$(file --mime $path/$item |awk '{gsub(/\n|;/,"");print$2}')
		allitem=$allitem$item"            "$mime" "
	done
	dialog --title "$path" \
	--menu "Select" 20 50 5 $allitem 2>fileinput
	
	if [ "$?" = "1" ]
	then
		showMenu
	else
		filechosen=$(cat fileinput)
		mimetwice=$(file --mime $path/$filechosen | \
		awk '{gsub(/\n|;/,"");print$2}')
		dir=$(echo $mimetwice|grep "directory"; echo $?)
		files=$(echo $mimetwice|grep "text"; echo $?)
		isText=""
		filepath=$path/$filechosen
		if [ "$mimetwice" = "inode/directory" ]
		then
			innerpath=$path/$filechosen;
			showFile $innerpath;
		elif [ "$mimetwice" = "text/plain" ]
		then
			isText="is"
			showInfo $filepath $isText $filechosen
		else
			isText="isnot"
			showInfo $filepath $isText $filechosen
		fi
		rm -f fileinput
	fi
}

showInfo(){
	rootpath=/fdroiddata
	path=$1
	istext=$2
	filename=$3
	size=$(ls -l $path |awk '{if($5<1000){print $5" Bytes"} \
	else if(1000<$5 && $5<1000000){print $5/1024" KB"} \
	else if(1000000<$5 && $5<1000000000){print $5/1024/1024" MB"} \
	else {print $5/1024/1024/1024" GB"} }')
	
	info=""
	innertext=""
	infotest=$(file $path)
	infocount=$(($(file $path | grep -o ' '|wc -l)+1))
	for i in $(seq 2 $infocount)
	do
		innertext=$(file $path | cut -d ' ' -f $i)
		info=$info$innertext" "
	done

	paths=$(echo ${path%/*})
	if [ "$istext" = "is"  ]
	then
		dialog --clear --extra-button \
		--extra-label edit \
		--msgbox "File name: $filename\nFile info: $info\
		\nFile size: $size" 20 50 
		if [ "$?" = "0" ]
		then
			showFile $paths
		elif [ "$?" = "1" ]
		then
			showEditor $path $paths
		fi
	else
		dialog --msgbox "File name: $filename\nFile info: $info\
		\nFile size: $size" 20 50
		if [ "$?" = "0" ]
		then
			showFile $paths
		fi	
	fi
}

showEditor(){
	path=$1
	paths=$2
	"${EDITOR:-vi}" $path
	showFile $paths
}

showCpuUse(){
	cpucount=$(($(sysctl -n hw.ncpu)+1))
	cpustr=$(top -P | grep "CPU" | \
	awk -F"," '{if(NR<cpucount)print $1$3" "$5"\n"}' cpucount=$cpucount)
	cpercent=$(top -P | grep "CPU"| \
	awk '{if(NR<c)per+=$3;} END {print per}' c=$cpucount)
	cden=$(($(echo "$cpercent"| cut -d '.' -f 1)+0))
	cpercent=$(($(echo "$cpercent"| cut -d '.' -f 2)+0))
	
	if [ $cpercent -gt 0 ]
	then
		cpercent=$(($cden+1))
	else
		cpercent=$cden
	fi

	dialog --title "CPU Usage" --gauge \
	"cpu loading\n$cpustr" 20 50 $cpercent
	
	if [ "$?" = "0" ]
	then
		showMenu
		exit
	fi
}

showMenu

